insert :: (Num a, Ord a) => a -> [a] -> [a] 
insert y [] = [y]
insert y (x:xs)
           | y <= x = [y]++[x]++xs 
           | otherwise = x:insert y xs

cnt y xs = foldl (\acc z -> if y==z then acc +1 else acc) 0 (xs) 

insert1 :: (Ord a) => a -> [a] -> [a]
insert1 x [] = [x]
insert1 x xs = foldr (\y acc-> if x > y  then (if (cnt x xs >= cnt x acc) then  [y] ++ [x] ++acc   else [y]++acc) else (if (length (acc++[y]) == length xs) then  [x] ++ [y] ++ acc else  [y] ++ acc) ) [] xs

insert2 :: (Ord a) => a -> [a] -> [a]
insert2 x [] = [x]
insert2 x xs = foldl (\acc y-> if x < y  then (if (cnt x xs >= cnt x acc) then acc ++ [x] ++ [y]   else acc ++ [y]) else (if (length (acc++[y]) == length xs) then acc ++ [x] ++ [y] else acc ++ [y]) ) [] xs
             
