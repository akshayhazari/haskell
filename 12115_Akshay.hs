l2i :: [Int] -> Int
l2i [] = 0
l2i xs = read $ foldl (\acc x-> acc ++ show x ) "" xs

l2i' :: [Int] -> Int
l2i' [] = 0
l2i' xs = foldr (\acc x-> 10 * x + acc) 0 (reverse (xs))

l2i'' :: [Int] -> Int
l2i'' [] = 0
l2i'' xs = foldl (\acc x-> 10 * acc + x) 0 (xs)
     
flatten :: [[a]]->[a]
flatten xs = foldl (\acc x-> acc ++ x ) [] xs 

flatten1 :: [[a]]->[a]
flatten1 xs = foldr (\x acc -> x ++ acc ) [] xs

insert :: ( Ord a) => a -> [a] -> [a] 
insert y [] = [y]
insert y (x:xs)
           | y <= x = [y]++[x]++xs 
           | otherwise = x:insert y xs
                         
cnt y xs = foldl (\acc z -> if y==z then acc +1 else acc) 0 (xs) 

insert1 :: (Ord a) => a -> [a] -> [a]
insert1 x [] = [x]
insert1 x xs = foldr (\y acc-> if x > y  then (if (cnt x xs >= cnt x acc) then  [y] ++ [x] ++acc   else [y]++acc) else (if (length (acc++[y]) == length xs) then  [x] ++ [y] ++ acc else  [y] ++ acc) ) [] xs

insert2 :: (Ord a) => a -> [a] -> [a]
insert2 x [] = [x]
insert2 x xs = foldl (\acc y-> if x < y  then (if (cnt x xs >= cnt x acc) then acc ++ [x] ++ [y]   else acc ++ [y]) else (if (length (acc++[y]) == length xs) then acc ++ [x] ++ [y] else acc ++ [y]) ) [] xs                        
                         
insort :: (Ord a) => [a]-> [a]
insort [] = []
insort (x:xs) = foldr (insert) [] (x:xs)             

del _ []=[]
del a (x:xs) = (if a==x then xs else x: (del a xs)) 

selsort :: (Ord a) => [a]->[a]
selsort [] =[]
selsort xs= [minimum xs] ++  (selsort (del (minimum xs) xs))

qsort :: (Ord a)=>[a] -> [a]
qsort []=[]
qsort (x:xs) = qsort (filter (\a -> if x >= a then True else False) xs) ++ [x] ++  qsort (filter (\a -> if x < a then True else False) xs)

myunzip :: [(a,b)] -> ([a],[b]) 
myunzip xs = foldr (\x acc -> puts (fst x) (snd x) acc) ([],[]) xs     
  where puts x y ([],[])=([x],[y])
        puts x y ([],_)=([],[])
        puts x y (_,[])=([],[]) 
        puts x y a = (x:(fst (a)),y:(snd (a)))
                 
length1 :: [a] -> Int                 
length1 []=0
length1 xs = foldr (\x acc-> 1 + acc) 0 xs  

myzip :: [a]->[b]->[(a,b)]
myzip [] _ =[]
myzip _ [] = []
myzip ls zs = foldr (\i acc-> i:acc) [] (newls ls zs)
  where newls _ [] = []
        newls [] _ = []
        newls (a:as) (b:bs) = (a,b):(newls as bs) 

aplus :: (Maybe Int,Int) -> Int -> (Maybe Int,Int)
aplus (Nothing,c) x = (Nothing,x+c)
aplus ((Just a),c) _ = (Just (a),c)

felem ::(Eq a) => a -> [a] -> Maybe Int
felem a [] = Nothing
felem a xs = fst $ foldr (\x acc -> if a/=x then aplus acc 1 else (Just (c acc),(c acc)) ) (Nothing,0) xs
             where c acc = z - (snd acc)
                   z = length xs
                     
  


