data Tree a = Leaf a | Node (Tree a) (Tree a) deriving (Show)

splitm :: Tree a->(Tree a,Maybe (Tree a))
splitm (Leaf a) = (Leaf a,Nothing)
splitm (Node l r) =
                   case splitm l of 
                     (a,Nothing) -> (a, Just r)
                     (a,Just b) -> (a,Just (Node b r))