import Data.List
primes :: (Integral a,Eq a) => [a] -> [a]
primes [] = []
primes (x:xs)
  | x == 1 = primes xs
primes (x:xs)=  let a = ((filter (/=0) $ zipWith (\a b -> if (a>0) then b else a) (map (flip rem x) xs) xs)) in (x: (primes a))
                   
primes1 []=[]
primes1 (x:xs) = x :primes1(filter ((0/=).(flip rem x)) xs)