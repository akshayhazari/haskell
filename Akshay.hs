aplus :: Maybe Int -> Maybe Int -> Maybe Int
aplus Nothing Nothing = Nothing
aplus (Just a)  Nothing = Nothing 
aplus Nothing (Just b) = Nothing
aplus (Just a) (Just b) = (Just (a+b))

eqq ::(Eq a)=> Maybe a -> Maybe a -> Bool
eqq Nothing Nothing = False 
eqq Nothing _ = False
eqq _ Nothing = False
eqq (Just a) (Just b) = (if a == b then True else False)

felem1 ::(Eq a) => a -> [a] -> Maybe Int
felem1 a [] = Nothing
felem1 a xs = eval $ foldr (\x acc -> if (((Just a) `eqq` (Just x)))  then (fst acc,True) else (if snd acc==True then acc else (fst acc `aplus` (Just 1),False))) ((Just 0),False) (reverse (xs))
             where eval (a,b) = if b==False then Nothing else a                   
                 
unzip1 :: [(a,b)] -> ([a],[b])
unzip1 xs = foldr (\x acc -> ((fst x):(fst acc),(snd x):(snd acc))) ([],[]) xs 
                  
             
