
data Tree a = Empty | Node a (Tree a) (Tree a) deriving (Show, Read, Eq)
treeIns :: (Ord a) => a -> Tree a -> Tree a 
treeIns x Empty= Node x (Empty) (Empty)
treeIns x (Node a l r)
  | x == a = Node a l r 
  | x < a = Node a (treeIns x l) r
  | x > a = Node a l (treeIns x r)

leftm :: Tree a -> Tree a          
leftm Empty = Empty            
leftm (Node a (Empty) (Empty)) = (Node a (Empty) (Empty))
leftm (Node a (l) (Empty)) = leftm l
leftm (Node a (l) (r)) = leftm l

rightm :: Tree a -> Tree a 
rightm Empty = Empty 
rightm (Node a (Empty) (Empty)) = (Node a (Empty) (Empty))
rightm (Node a (Empty) (r)) = rightm r
rightm (Node a (l) (r)) = rightm r

-- It gives an error for delete root node else maybe it works. It shows an error like Irrefutable pattern. What I have tried is if x equals the node to be deleted then replace it with either leftmost of right or rightmost of left whichever is available and then delete the rightmost or leftmost after updation.
 
delt :: (Eq a, Ord a)=>a -> Tree a -> Tree a
delt x Empty = Empty
delt x (Node a (Empty)(Empty)) 
  | x== a = Empty
delt x (Node a l r)
  |x == a = (if l /= (Empty) then (let (Node rm (Empty) (Empty)) = rightm l in (Node rm (delt rm l) r)) else (let (Node rm (Empty) (Empty)) = leftm r in (Node rm l (delt rm r)) ))
  |x>a = Node a (l) (delt x r) 
  |x < a = Node a (delt x l ) r
